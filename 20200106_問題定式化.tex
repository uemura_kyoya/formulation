\documentclass[A4paper,twocolumn, 10pt]{jsarticle}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cases}
\usepackage{bm}
\usepackage[dvipdfmx]{graphicx}
\title{TEST2帯観測モデルに基づくスパース推定} 
\author{情報学研究科システム科学専攻適応システム論分野\\修士1回生 \ 上村京也}
\begin{document}
\maketitle

\noindent \large \bf 1. はじめに\\

\noindent \normalsize 1.1 スペクトル分析
\par \rm スペクトル分析は，分析対象のスペクトルからその構成要素を定性的・定量的に評価する手法である．ここでのスペクトルとは，複雑な組成を持つ観測量を成分ごとに分解して並べたデータ配列を指す．スペクトル分析の例としては分光分析や質量分析が挙げられ，天文学や生化学など幅広い分野で用いられている（図1）．また，近年では分析対象の複雑化に伴い，強度の非常に弱いスペクトルシグナルを短時間で感度良く測定することが求められている \cite{msms}．一方で，一般に測定時間と感度はトレードオフの関係にあるため，これらを両立する測定手法の確立は大きな課題となっている．
\begin{figure}[h]
\begin{center}
  \includegraphics[width=7.5cm]{Figure1.png}
  \caption{質量分析（MS）スペクトル．}
\end{center}
\end{figure}

\noindent \bf 1.2 スパーススペクトル
\par \rm 本研究では配列要素の多くがゼロであるようなスペクトル，すなわちスパーススペクトルに注目する．スパーススペクトルは分析対象が固有の値（例えば質量や電荷，エネルギー準位など）を持つ場合に現れるスペクトルであり，図1に示したMSスペクトルや分光分析における輝線スペクトルが代表的な例として挙げられる．\\

\noindent \bf 1.3 従来のスペクトル測定について
\par \rm 分光分析を例に一般的なスペクトル測定の概略を示す（図2）．分光分析では，まず様々な成分（波長）が混ざった光をプリズム等によって分解する．次にスリットを用いて光を単一波長のみにしぼり，これを検出器で計測する．この操作で測定範囲をスキャンすることによって目的のスペクトルを得る．\\
\begin{figure}[h]
\begin{center}
  \includegraphics[width=7.5cm]{Figure2.png}
  \caption{分光分析におけるスペクトル測定の模式図．}
\end{center}
\end{figure}

ここで，$n$成分に離散化されたスペクトルが成すベクトルを$\bm{\beta} \in \mathcal{R}^n$とすると，上で示したスペクトルの測定過程は次のような線形モデルで表現できる．
\begin{eqnarray}
\bm{y} = \bm{X\beta} + \bm{\epsilon}
\end{eqnarray}
$\bm{y}=(y_1, y_2, \cdots, y_n)^{\top}$は測定によって得られた$n$次元出力ベクトルを，$\bm{\epsilon}$は$n$次元測定ノイズをそれぞれ表している．また，$n \times n$の観測行列$\bm{X}$はプリズムとスリットの特性を表し，上記分光分析の場合は$\bm{X} = \bm{I}_n$となる．\\

\noindent \large \bf 2. 提案手法\\
\normalsize \par \rm 図2から明らかなように，従来のスペクトル測定プロセスでは1回の観測に対して1成分のみ計測しており，その他の成分の情報は完全に捨てられている．このことから，これらの情報を利用したスペクトル測定を行うことができれば，測定の短時間・高感度化が期待できると考えられる．また，対象とするスペクトルがスパースである場合，この情報もスペクトルの推定に利用可能である．これらの観点から本研究では，式(1)の観測行列に帯行列$\bm{X}$：
\begin{eqnarray*}
x_{ij}=
\begin{cases}
1 & (0 \leq j - i \leq w)\\
0 & (otherwise)
\end{cases}
\end{eqnarray*}
を用いてスペクトル測定を行い（以降帯観測と呼ぶ），スパース推定を用いてスペクトル$\bm{\beta}$を推定する手法を提案する．ここで$w$は帯行列のバンド幅を表す．観測行列に帯行列を選択することで得られるメリットとしては，実際の測定系への導入が非常に容易であることが挙げられる．なぜなら帯観測モデルにおいてバンド幅$w$を大きくすることは，図2の測定系においてスリットの幅を大きくすることに対応するためである（図3）．また$w = 0$のとき帯行列は単位行列に一致するため，帯観測モデルは従来の測定手法の自然な拡張となっている．\\

\begin{figure}[h]
\begin{center}
  \includegraphics[width=7.5cm]{Figure3.png}
  \caption{帯観測モデルに基づくスペクトル測定の模式図．}
\end{center}
\end{figure}

帯観測モデル（式(1)）においてバンド幅$w \ge 0$が大きくなると，$\frac{||\bm{X\beta}||_{2}}{||\bm{\epsilon}||_{2}}$すなわち信号対雑音比が向上する．一方で，バンド幅$w$が大きくなると観測行列の直交性の低下やランク落ちが起こり，スペクトル$\bm{\beta}$の推定精度低下の要因となる．このように帯観測モデルは正と負どちらの影響も与えうるため，実際のスペクトル推定に対する有効性が不明瞭である．そこで，実際にバンド幅$w$がスペクトル推定にどのような影響を与えるかを調べるために数値実験を行なった．\\

\noindent \large \bf 3. 数値実験\\

\noindent \normalsize 3.1 問題設定
\par \rm 本研究では先に述べたように，以下の線形観測モデルを考える．
\begin{equation}
\bm{y} = \bm{X\beta} + \bm{\epsilon} \tag{1}
\end{equation}
\begin{center}
\begin{table}[h]
\begin{tabular}{ll}
$\bm{y} \in \mathcal{R}^n$ &: Vector of outcomes\\
$\bm{X} = (x_{ij}) \in \mathcal{R}^{n\times n}$ &: Measurement matrix\\
$x_{ij}=
\begin{cases}
1 & (0 \leq j - i \leq w)\\
0 & (otherwise)
\end{cases}$&: Band matrix\\
$w$ &: Bandwidth\\
$\bm{\beta} \in \mathcal{R}^n \ \ (||\bm{\beta}||_0 \ll n)$&: Sparse spectrum\\
$J = \{j | \beta_j \neq 0\}$&: Active set\\
$\bm{\epsilon}\sim N(\bm{0}, \sigma^2\bm{I}_n)$&: Vector of noises
\end{tabular}
\end{table}
\end{center}
上記の帯観測モデルについて，スペクトル$\bm{\beta}$はスパースである$(||\bm{\beta}||_0 \ll n)$ことを仮定しているため，$\bm{\beta}$の推定には代表的なスパース推定手法であるLasso回帰を用いた（式(2)）．
\newcommand{\argmin}{\mathop{\rm arg~min}\limits}
\begin{eqnarray}
\argmin_{\bm{\beta}} \ \left[\frac{1}{2}||\bm{y} - \bm{X\beta}||^2_{2} + \lambda||\bm{\beta}||_{1}\right]
\end{eqnarray}
また，比較として同じ正則化回帰の一種であるRidge回帰による推定も同様にして行った（式(3)）．
\begin{eqnarray}
\argmin_{\bm{\beta}} \ \left[\frac{1}{2}||\bm{y} - \bm{X\beta}||^2_{2} + \lambda||\bm{\beta}||^2_{2}\right]
\end{eqnarray}

数値実験の中で調節可能としたパラメータは以下の5つであり，ノイズの標準偏差は$\sigma_{noise} = 1$とした．実験に用いたスペクトルは，ランダムに生成した非ゼロ要素の添字集合$J$を用いて人工的に生成した．
\begin{center}
\begin{table}[h]
\begin{tabular}{ll}
$\lambda$ &: Penalty parameter\\
$w$ &: Bandwidth\\
$n$ &: Dimension of spectrum\\
$\beta_j$ &: Signal strength\\
$||\bm{\beta}||_{0}$ &: Number of nonzero element
\end{tabular}
\end{table}
\end{center}
\noindent \bf 3.2 正則化パラメータ$\lambda$の決定
\par \rm 今回用いたLasso回帰（式(2)）とRidge回帰（式(3)）それぞれの正則化パラメータ$\lambda$は，回帰における正則化項の寄与の大きさを決める重要なパラメータであり，適切な推定結果を得るためには適切な値に設定する必要がある．ここでは，他の実験パラメータ$w, n, \beta_j, ||\bm{\beta}||_{0}$を決め，目的関数を$||\bm{\beta} - \hat{\bm{\beta}}||^2_2$としてガウス過程回帰とUCB探索を組み合わせたベイズ最適化を用いることで，最適な正則化パラメータ$\lambda_{opt}$を決定した．

\end{document}